The Unicodifier
===============

A work-around for the broken `utf8mb3` encoding used by old versions of MySQL.

The Problem
-----------

Old versions of MySQL assumed that `utf8` text wouldn't include characters with
a Unicode code-point above 65535 (`0xFFFF` in hexadecimal) since the developers
apparently only cared about supporting the Basic Multilingual Plane:

<https://en.wikipedia.org/wiki/Basic_Multilingual_Plane>

As a consequence, MySQL couldn't handle (most) emoji without breaking somehow.

Newer versions of MySQL add `utf8mb4` as an alternative to `utf8` (which is now
known as `utf8mb3`) but old databases need to be manually converted over to it,
otherwise the original (broken) `utf8` encoding will continue to be used.

The Solution
------------

HTML has a feature called Character Entity References:

<https://en.wikipedia.org/wiki/Character_entity_references>

Some of them have "names" (such as `&amp;`), but it's possible to specify _any_
code-point as a numerical value, such as `&#38;` or `&#x26;`.

It's therefore possible to convert UTF-8 text into a MySQL-compatible format by
replacing any unsupported characters with a character entity reference; the end
result will look identical when rendered by a web browser.

This is what the Unicodifier does: Simply enter your text, and press "Convert".

License
-------

This code is made available under the [MIT](http://opensource.org/licenses/MIT)
licence, allowing you to use or modify it for any purpose, including commercial
and closed-source projects. All I ask in return is that _proper attribution_ is
given (i.e. don't remove my name from the copyright text in the source code).
